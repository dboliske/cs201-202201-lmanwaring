package labs.lab2;

import java.util.Scanner;

public class GradeAverage {

	public static void main(String[] args) {
		
		//create Scanner
		Scanner input = new Scanner (System.in);
		
		//Prompts user for initial grade
		System.out.print("Enter a Grade. When done, enter -1: ");
		float x = input.nextFloat();
		
		float total = 0; // sets total points to zero
		int counter = 0; // sets counter to 0
		
		
		while (x != (-1)) {
			counter = (counter + 1);									//counts number of grades
			total = (total + x); 										//calculates total points
			System.out.print("Enter a grade. When done, enter -1: "); 	//asks for another grade
			x = input.nextFloat();										//sets x to new grade or -1
					
		}
		float average = (total/counter);								//calculates average
		System.out.println("The average grade is " +average+ "%");		//displays average to counsel
		
		//close Scanner
		input.close();
	}

}
