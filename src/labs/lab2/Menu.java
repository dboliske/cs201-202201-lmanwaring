package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		
		//create scanners
		Scanner input = new Scanner (System.in);
		Scanner loop = new Scanner (System.in);
		
		//set boolean to true
		boolean running = true;
		
		while (running ) {
		//show menu options
		System.out.println("a. Say Hello");
		System.out.println("b. Addition");
		System.out.println("c. Multiplication");
		System.out.println("d. Exit");
		
		//prompt user for choice
		System.out.print("Choice: ");
		String in = input.nextLine();
		
		switch (in) {
		
		//Option A
		case "a":
		System.out.println("Hello");				// prints hello
		break;
		
		// Option B and C
		case "b" :case "c":
		System.out.print("Enter a number: ");		//prompts user for number
		float x = loop.nextFloat();					//stores first value
		System.out.print("Enter another number: "); //prompts user for second number
		float y  = loop.nextFloat();				//stores second value
		if (in.equals("b")) {						
			float sum = (x+y);						
			System.out.println(sum);				//displays sum
		}else{
			float product = (x*y);
			System.out.println(product);			//displays product
		}
		break;
		
		
		//Option D
		case "d":
			running = false;						// changes boolean variable to end while loop
		break;
		
		//Not an Option
		default:
			System.out.println("Not an option");	// when invalid option
		break;
		
		
			}
		
		}
		//Close Scanners
		input.close();								//input
		loop.close(); 								//loop
	}

}
