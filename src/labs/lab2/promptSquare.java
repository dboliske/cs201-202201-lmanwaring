package labs.lab2;

import java.util.Scanner;

public class promptSquare {

	public static void main(String[] args) {
		
		//create Scanner
		Scanner input = new Scanner (System.in);
		
		//prompt user for size of the square
		System.out.print("Enter an Integer: ");
		int x = input.nextInt();
		
		//loop to print square
		for (int i=0; i<x; i++) {
			for (int j=0; j<x; j++) {
				System.out.print("* ");		// prints one row
			}
			System.out.println(); 			// creates new row
		}
		
		//close Scanner
		input.close();

	}

}
