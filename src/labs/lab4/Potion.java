package labs.lab4;

public class Potion {

	//instance variables
	private String name;
	private double strength;
	
	//default constructor
	public Potion() {
		name = "invisibility";
		strength = 8;
	}
	
	//non-default constructor
	public Potion(String x, double y){
		name = "invisibility";
		setName(x);
		strength = 8;
		setStrength(y);
	}
	
	//mutator methods
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	//get methods
	public String getName() {
		return name;
	}
	
	public Double getStrength(){
		return strength;
	}
	
	
	//to String
	public String toString() {
		 return "Potion: " + name + " Strength: " + strength + "X";
	 }
	
	//boolean methods
	 public boolean equals(Potion d) {
		 if (this.name != d.getName()) {
			 return false;
		 } else if (this.strength != d.getStrength()) {
				 return false;
		 }
		 return true;
	 }
	 
	 public boolean validStrength(double strength) {
		 if (strength <= 1 && strength >= 10) {
			 return true;
		 }else {
			 return false;
		 }
	 }
	
}
