package labs.lab4;

public class GeoLocation {
	
	//create variables
	 private double lat;
	 private double lng;
	 
	 //default constructor
	 public GeoLocation() {
		 lat = 14.113;
		 lng = 98.222;
	 }
	 
	 //non default constructor
	 public GeoLocation(double x, double y) {
		 lat = 27;
		 setLat(x);
		 lng = 177;
		 setLng(y);
	 }
	 
	 //mutator methods
	 public void setLat(double lat) {
		 if (lat >= -90 && lat <= 90) {
		 this.lat = lat;
		 }
	 }
	 
	 public void setLng(double lng) {
		 if (lng >= -180 && lng <= 180) {
		 this.lng = lng;
		 }
	 }
	 
	 //get methods
	 public double getLat() {
		 return lat;
	 }
	 
	 public double getLng() {
		 return lng;
	  }
	 
	 //calcMethods
	 
	public double calcDistance(GeoLocation newLocation) {
		double distance = Math.sqrt(Math.pow(lat-newLocation.getLat(),2)+Math.pow(lng-newLocation.getLng(),2));
		return distance;
	}
	public double calcDistance(double lat2,double lng2) {
		double distance = Math.sqrt(Math.pow(lat-lat2,2)+Math.pow(lng-lng2,2));
		return distance;
	}
	 
	 //toString method
	 public String toString() {
		 return "(" + lat + "," + lng + ")";
	 }
	 
	 //equals boolean
	
		public boolean equals(GeoLocation d) {
			 if (this.lat != d.getLat()) {
				 return false;
			 } else if (this.lng != d.getLng()) {
					 return false;
			 }
			 return true;
		}
		
	//validation booleans	
	public boolean validLat(double lat) {
		if(lat >= -90 && lat<=90) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean validLng(double lng) {
		if(lat >= -90 && lat<=90) {
			return true;
		}else {
			return false;
		}
	}

		
	 }

	 
	 

