package labs.lab4;

public class PhoneNumberTester {

	public static void main(String[] args) {
		
		PhoneNumber n = new PhoneNumber();
		PhoneNumber n2 = new PhoneNumber("1","222","3334444");
		
		//default constructor
		System.out.println(n.toString());
		
		//non-default constructor
		System.out.println(n2.toString());

	}

}
