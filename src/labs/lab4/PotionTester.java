package labs.lab4;

public class PotionTester {

	public static void main(String[] args) {
		
		Potion p = new Potion();
		Potion p2 = new Potion("flight", 3);
		
		//default constructor
		System.out.println(p.toString());
		
		//non-default constructor
		System.out.println(p2.toString());
		
	}

}
