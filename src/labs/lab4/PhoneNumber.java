package labs.lab4;

public class PhoneNumber {
	
	//create variables
	private String countryCode;
	private String areaCode;
	private String number;
	
	//default constructor
	public PhoneNumber() {
		countryCode = "1";
		areaCode = "123";
		number = "1231234";
	}
	
	//non-default constructor
	public PhoneNumber(String x, String y, String z) {
		countryCode = "1";
		setCountryCode(x);
		areaCode = "123";
		setAreaCode(y);
		number = "1231234";
		setNumber(z);			
	}
	
	//mutator methods
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	//get methods
	public String getCountryCode(){
		return countryCode;
	}
	public String getAreaCode(){
		return areaCode;
	}
	public String getNumber(){
		return number;
	}
	
	//toString method
	public String toString() {
		 return "+" + countryCode + "(" + areaCode + ")" + number;
	 }
	
	//validation (boolean) methods
	
	public boolean validAreaCode(String areaCode) { //???
		if (areaCode.length() == 3 ) {
			return true;
		}
		return false;
	}
	
	public boolean validNumber(String number) { //???
		if (number.length() == 7) {
			return true;
		}	
		return false;
	}
	
	
	public boolean equals(PhoneNumber d) {
		 if (this.countryCode != d.getCountryCode()) {
			 return false;
		 } else if (this.areaCode != d.getAreaCode()) {
				 return false;
		 } else if (this.number != d.getNumber()) {
			 		return false;
		 }
		 return true;
		 }

		 

} // end 
