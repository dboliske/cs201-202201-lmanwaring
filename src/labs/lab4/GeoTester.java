package labs.lab4;

public class GeoTester {

	public static void main(String[] args) {
		
		GeoLocation location = new GeoLocation();
		GeoLocation location2 = new GeoLocation(78,23);
		
		//default constructor coordinates
		System.out.println(location.getLat());
		System.out.println(location.getLng());
		
		//non-default constructor coordinates
		System.out.println(location2.getLat());
		System.out.println(location2.getLng());

	}

}
