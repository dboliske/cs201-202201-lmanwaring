package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class storenumbers {

	public static void main(String[] args) {
		
		Scanner a = new Scanner(System.in);										//create scanner
		System.out.println("Enter a number. When finished, enter 'Done':  ");	//prompt user for a number
		String entered = a.nextLine();											//create string
		
		
		int counter = 0;				//create counter for # of subscripts
		int[] values = new int[1000];	//create initial array
		
		
		while (!(entered.equals("Done"))) {			//while the user hasn't entered "Done"
		 int num = Integer.parseInt(entered);		//convert strings to integers
		 values[counter] = num;						//add number to array
		 counter = (counter+1);
		 System.out.println("Enter a number: ");	    //prompt user again
		 entered = a.nextLine();						//prompt user again
		}
		
		
		
		int[] valuesCopy = new int[counter];		//create temporary array w/ # of subscripts
		
		for (int i=0; i<valuesCopy.length;i++) {
			valuesCopy[i] = values[i];				//transfer values of initial array to temporary array
		}
		
		values = valuesCopy;						//assign arrays to same data set
		valuesCopy= null;
		
		
		System.out.println("File name: ");					//prompt user for file name 
		String name = a.nextLine();						
		
		try {
		
			
			FileWriter newFile = new FileWriter(name);		//create fileWriter
			
			
			for (int i = 0; i<values.length; i++) {
			newFile.write(Integer.toString(values[i]) + "\n"); 	//add values to file
			
			}
			
			newFile.flush();
			newFile.close();
			
		} catch (IOException e) {
			System.out.println("Error");
		}
		
		System.out.println("Saved to " + name );				//End message	
		a.close();												//close Scanner							
		
		}

	}

	


