package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class grades {

	public static void main(String[] args) throws IOException {
	
		File f = new File("src/labs/lab3/grades.csv");	
		Scanner input = new Scanner(f);					
		
		int[] grades = new int[14];						//create array 
		int a =0;										//set counter to 0
		
		while (input.hasNextLine()) {
			String NameGrade = input.nextLine();		//creates strings from data
			String[] values = NameGrade.split(","); 	//splits the values at the comma
			grades[a] = Integer.parseInt(values[1]);	//converts to integer
			a++;
		}
		
		input.close();								//closes scanner
		
		
		int total = 0;								//sets initial total to 0
		
		for (int i = 0; i<grades.length; i++) {
			total = (grades[i]+total);
		}
			
		int average = (total/grades.length);		//computes average
		System.out.print(average);					//prints out average
		
		
			
		}
		
		
	}




