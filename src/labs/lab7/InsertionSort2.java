package labs.lab7;

public class InsertionSort2 {
	//DONE
	//InsertionSort
	public static String[] sort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}
	
	public static void main(String[] args) {
	//create array	
	String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
	
	//implement sort method
	sort(array);
	
	//print new sorted array
	for (int i=0; i<array.length; i++) {
		System.out.println(array[i]);
	}
	}

}
