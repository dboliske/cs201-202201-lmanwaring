package labs.lab7;

public class BubbleSort1 {
	
	public static int[] swap(int[] numbers) {
		boolean done = false;
		do {
			done = true;
			for (int i=0; i<numbers.length - 1; i++) {
				if ((numbers[i+1] < numbers[i] )) {
					int temp = numbers[i+1];
					numbers[i+1] = numbers[i];
					numbers[i] = temp;
					done = false;
				}
			}
		} while (!done);
		
		return numbers;
	}

	public static void main(String[] args) {
	
	//create array
	int[] numbers = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
	
	//perform number swap
	swap(numbers);
	
	//print out each 
	for (int i =0; i<numbers.length; i++) {
		System.out.println(numbers[i]);
	}

	}

}
