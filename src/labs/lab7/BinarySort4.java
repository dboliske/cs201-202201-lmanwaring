package labs.lab7;

import java.util.Scanner;

public class BinarySort4 {
	//DONE
	
	public static int search(String[] array, String value) {
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end) / 2;
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return pos;
	}
	
	public static void main(String[] args) {
		
		//create array
		String[] langs = {"c", "html", "java", "python", "ruby", "scala"};
		
		//create Scanner
		Scanner input = new Scanner(System.in);
		
		//prompt user for input
		System.out.print("Enter a languge: ");
		String term = input.nextLine();
		
		//print position in array
		System.out.print(search(langs, term));
		
		//close Scanner
		input.close();
		
		
		
		
		
		

	}

}
