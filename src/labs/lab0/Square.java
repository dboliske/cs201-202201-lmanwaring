// Lila Manwaring, CS 201, Section 1, January 19 2022

package labs.lab0;

public class Square {

	public static void main(String[] args) {
		
		// print each row of square individually w/ System.out.println()
	
	// print out top row
	System.out.println("XXXXXX");
	
	// print out middle rows
	System.out.println("X    X");
	System.out.println("X    X");
	System.out.println("X    X");
	
	// print out bottom row
	System.out.println("XXXXXX");
			
	// prints a rectangle, but super inefficient
		
	}

}
