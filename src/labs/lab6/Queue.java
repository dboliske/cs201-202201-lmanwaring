package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Queue {
	
	
	static boolean exit = false;
	static ArrayList<Customer> queue = new ArrayList<Customer>(); //create array "queue"
	
	//add customer method
	public static void addCustomer(Customer newCustomer) {
		
		queue.add(newCustomer);
		
		int counter = 0;
		for (int i=0; i<queue.size(); i++) {
			counter = counter + 1;
		}
		
		System.out.println(newCustomer.getName() + " is customer number " + counter);
	}
	
	//remove customer methods
	public static void removeCustomer() {
		
		System.out.println(queue.get(0));
		queue.remove(0);
	}
	
	//exit method
	public static void leave() {
		exit = true;
	}
	
	//main method w/ menu
	public static void main(String[] args) {
				
				
				Scanner choice = new Scanner(System.in);
		
		do {
			System.out.println("1. Add Customer to Queue");
			System.out.println("2. Help Customer");
			System.out.println("3. Exit");
			
			System.out.print("Choose a Command: ");
			String c = choice.nextLine();
			
			switch (c) {
			
			case "1": 
				System.out.print("Enter Customer Name: ");
				
				String person = choice.nextLine();
				Customer customer = new Customer(person);
				addCustomer(customer);
				
				break;
				
				
			case "2":
				
				removeCustomer();
				
				break;
				
			case "3":
				leave();
				break;
				
				default:
					System.out.print("Not a Choice");
			}
			
				
				
		}while(exit == false);
		
		choice.close();
		
	}
	
	
	


}
