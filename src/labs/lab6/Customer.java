package labs.lab6;

public class Customer extends Queue {
	
	//variables
	private String name;
	
	
	//default
	public Customer() {
		name = "john";
	}
	
	//non default
	public Customer(String s) {
		name = "john";
		setName(s);
				
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	//toString
	 public String toString() {
		 return "Customer: " + name;
	 }
	
	
	
	
		
		
	}
	


