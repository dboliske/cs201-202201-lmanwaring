package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStation extends GeoLocation{
	
	//create variables
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	
	//default construtor
	public CTAStation(){
		super();
		name = "name";
		location = "location";
		wheelchair = true;
		open = true;
				
	}
	
	//non default constructor
	public CTAStation(String n, String l, boolean w, boolean o, double lat, double lng) {
		super(lat,lng);
		this.name = n;
		this.location = l;
		this.wheelchair = w;
		this.open = o;
	}

	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isWheelchair() {
		return wheelchair;
	}
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}

	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	//toString
	 public String toString() {
		 return name + ", " + location + ", Wheechair Accesible? :" + wheelchair + ", Open? : " + open;
				 
				
	 }
	 
	 //equals
	 public boolean equals(CTAStation d) {
		 if (this.name != d.getName()) {
			 return false;
		 } else if (this.location != d.getLocation()) {
				 return false;
		 } else if (this.wheelchair != d.isWheelchair()) {
			 return false;
		 }else if (this.open != d.isOpen()) {
			 return false;
		 }else
		 return true;
	 }
	
}
