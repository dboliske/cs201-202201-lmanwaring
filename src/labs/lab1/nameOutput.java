// Lila Manwaring, CS 201, Section 1, January 23 2022

package labs.lab1;

import java.util.Scanner;

public class nameOutput {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		System.out.print("What is you name?: "); // asks users for name
		
		String value = input.nextLine(); //assigns name to value
		
		System.out.println(value); // echos name
		
		input.close();

	}

}
