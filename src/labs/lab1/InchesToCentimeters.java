// Lila Manwaring, CS 201, Section 1, January 23 2022

package labs.lab1;

import java.util.Scanner;

public class InchesToCentimeters {

	public static void main(String[] args) {
		
		//create scanner
		Scanner input = new Scanner (System.in);
		
		//prompt user for inches
		System.out.println("Inches: ");
		float in = input.nextFloat();
		
		//conversion
		double cm = (in * 2.54);
		
		//print results
		System.out.println(in + " inches is " + cm + " centimeters");
		
		//test plan
		//input: 0 expected output: 0 actual output: 0
		//input: 0 expected output: 25.4 actual output: 25.4
		//input: 0 expected output: 50.8 actual output: 50.8
		

	}

}
