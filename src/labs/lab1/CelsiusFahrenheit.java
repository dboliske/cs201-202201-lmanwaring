// Lila Manwaring, CS 201, Section 1, January 23 2022

package labs.lab1;

import java.util.Scanner;

public class CelsiusFahrenheit {

	public static void main(String[] args) {
		
		// create scanner
		Scanner input = new Scanner (System.in);
		
		//prompt user for farenheit
		System.out.print("Enter a Farenheit: ");
		float f = input.nextFloat();
		
		//prompt user for celcius
		System.out.print("Enter a Celsius: ");
		float c = input.nextFloat();
		
		// conversions of f to c and c to f
		float FtoC = ((f-32)*(5/9));
		float CtoF = ((c*(9/5))+32);
		
		// print results of conversions
		System.out.println((FtoC)+ " celcius");
		System.out.println((CtoF)+ " farenheit");
		
		//test plan
		//input: 0F/0C      expected output: -17.78C/32F actual output: 0C/32F
		//input: 20F/20C    expected output: -6.66C/54F  actual output: 0C/54F
		//input: 100F/100C  expected output: 37.77C/132F actual output: 0C/132F
		
		// not working for farenheit to celsius
		
		
		
		
		
		
		
		

	}

}
