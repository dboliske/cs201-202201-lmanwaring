// Lila Manwaring, CS 201, Section 1, January 23 2022

package labs.lab1;

import java.util.Scanner;

public class boxCalc {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		// prompt user for height
		System.out.println("What is the height of the box?: ");
		float h = input.nextFloat();
		
		// prompt user for width
		System.out.println("What is the width of the box?: ");
		float w = input.nextFloat();
		
		// prompt user for depth
		System.out.println("What is the depth of the box?: ");
		float d = input.nextFloat();
		
		// calculations
		float faceA = (h*w);                 //height and width
		float faceB = (h*d);                 //height and depth
		float faceC = (w*d);                 //width and depth
		float total = (faceA+faceB+faceC)*2; //total surface area
		
		// print results
		System.out.println("The box needs " + total + " square inches of wood");
		
		//test plan
		//input: 2x2x2 expected output: 24  actual output: 24.0
		//input: 1x1x1 expected output: 6   actual output: 6.0
		//input: 7x7x7 expected output: 294 actual output: 294.0

	}

}
