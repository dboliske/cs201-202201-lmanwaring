// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;
//DONE
public class Sorting {
	public static String[] sort(String[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j].compareTo(array[min])<0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		
	//create array
	String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
	
	//sort array of words
	sort(words);

	//print out each index of array
	for (int i =0; i<words.length; i++) {
		System.out.println(words[i]);
	
	}

}
}



		
	


