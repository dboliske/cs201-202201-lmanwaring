// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {

	public static void main(String[] args) {
		ArrayList<Integer> number = new ArrayList<Integer>();
		
		Scanner a = new Scanner(System.in);										//create scanner
		System.out.println("Enter a number. When finished, enter 'Done':  ");	//prompt user for a number
		String entered = a.nextLine();											//create string
		
		
		
		
		while (!(entered.equals("Done"))) {			//while the user hasn't entered "Done"
		 int num = Integer.parseInt(entered);		//convert strings to integers
		 number.add(num);						//add number to array
		 System.out.println("Enter a number: ");	    //prompt user again
		 entered = a.nextLine();						//prompt user again
		}
		int max = number.get(0);
		int min =number.get(0);
		
		for (int i = 0; i<number.size(); i++) {
			if (number.get(i)<min) {
				min = number.get(i);
			}
		}
		for (int i = 0; i<number.size(); i++) {
			if (number.get(i)>max) {
				max = number.get(i);
			}
		}
		System.out.println("The max is: "+max+"\n"+"The min is: "+min);
		
		a.close();
		
		}
			
		}
		
	


