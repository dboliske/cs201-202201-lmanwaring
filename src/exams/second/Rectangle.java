// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

public class Rectangle extends Polygon{

	private double width;
	private double height;
	
	public Rectangle(){
		width = 56;
		height = 40;
	}
	
	@Override
	public String toString() {
		return "Rectangle [width = " + width + ", height = " + height + "area = " + area() + "perimeter = " + perimeter() + "]";
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
				
	}

}
