// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

public abstract class Polygon {

	private String name;
	
	public Polygon() {
		name = "poly";
	}
	
	public Polygon(String n) {
		name = "poly";
		setName(n);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	 public String toString() {
		 return name;
	 }
	 
	 abstract public double area(); 
	 
	 public abstract double perimeter();
		 
	 
}
