// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

public class Classroom {
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "building";
		roomNumber = "27";
		seats = 90;
	}
	
	public String toString() {
		return "Classroom [building=" + building + ", roomNumber=" + roomNumber + ", seats=" + seats + "]";
	}

	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		if (seats>=0) {
		this.seats = seats;
	}else {
		seats = 90;
	}
}

}
