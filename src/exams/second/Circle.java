// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

public class Circle extends Polygon{

	private double radius;
	
	@Override
	public String toString() {
		return "Circle [radius = " + radius + "area = " + area() + "perimeter = " + perimeter() + "]";
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double area() {
		return  Math.PI * radius * radius;
	}

	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}

}
