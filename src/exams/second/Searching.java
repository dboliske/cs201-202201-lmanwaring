// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

import java.util.Scanner;
//DONE
public class Searching {
	public static int searchItem(Double[] list, String s){

		for (int i=0; i<list.length; i++) {
			Double itemName = list[i];
			
			if(s.equalsIgnoreCase(Double.toString(itemName))) {
				return i;
			}
			
		}
		return -1;
	}	
	
	public static void main(String[] args) {
		Double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner in = new Scanner(System.in);
		System.out.print("Enter Item: ");
		String desiredItem = in.nextLine();
		
		int index = searchItem(numbers,desiredItem);
		
		if (index == -1) {
			System.out.println("-1");
		}else {
			System.out.println(desiredItem + " is at index " + index);
		}
		
		in.close();
	}

}
