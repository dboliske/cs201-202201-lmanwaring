// Lila Manwaring, CS 201, Section 1, April 29 2022
package exams.second;

public class ComputerLab extends Classroom{

	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return "Classroom [building=" + building + ", roomNumber=" + roomNumber + ", seats=" + super.getSeats() + "computers=" + computers + "]";
	}
}
