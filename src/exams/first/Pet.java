package exams.first;

import labs.lab4.GeoLocation;

public class Pet {
	//create variables
		 private String name;
		 private int age;
		 
		 //default constructor
		 public Pet() {
			 name = "john";
			 age = 8;
		 }
		 
		 //non default constructor
		 public Pet(String x, int y) {
			 name = "john";
			 setName(x);
			 age = 8;
			 setAge(y);
		 }
		 
		 //mutator methods
		 public void setName(String name) {
			 
			 this.name = name;
			 
		 }
		 
		 public void setAge(int age) {
			 if (age > 0) {
			 this.age = age;
			 }
		 }
		 
		 //get methods
		 public String getName() {
			 return name;
		 }
		 
		 public int getAge() {
			 return age;
		  }
		 
		 //toString method
		 public String toString() {
			 return "Name: " + name + "Age: " + age;
		 }
		 
		 //equals boolean
		
			public boolean equals(Pet obj) {
				 if (this.name != obj.getName()) {
					 return false;
				 } else if (this.age != obj.getAge()) {
						 return false;
				 }
				 return true;
			}
			
		
			
		

			
		 }

		 
		 



