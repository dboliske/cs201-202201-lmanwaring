package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		int hw = input.nextInt();
		
		int w = 1;
		int h = hw;
		
		for (int i =0; i < h; i++) {
			for(int j = 0; j<w; j++) {
				System.out.print("*");
		}
		System.out.println();
		w = w+1;
		}
	
		
		
		input.close();
	}

}
