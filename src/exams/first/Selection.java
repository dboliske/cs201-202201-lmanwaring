package exams.first;

import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter an integer: ");
		int num = input.nextInt();
		
		
		if (num % 2 == 0 && num%3 != 0) {
			System.out.print("foo");
		}
		
		if (num % 2 != 0 && num % 3 == 0) {
			System.out.print("bar");
		}

		if (num % 2 == 0 && num%3 == 0) {
			System.out.print("foobar");
		}
		
		input.close();

	}

}
