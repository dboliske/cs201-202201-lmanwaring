// Lila Manwaring, CS 201, Section 1, April 29 2022
// This class is a subclass of the parent class "Item", that deals with items that have an expiration date

package project;


public class ProduceItem extends Item { 
	
	//expiration date variable
	protected String itemDate;
	
	//default constructor
	public ProduceItem(){
		super();
		setItemDate("02/10/2000");
	}
	
	//non default constructor 
	public ProduceItem(String name, String price, String experation) {
			super(name, price);
			setItemDate(experation);
		
	}

	//getter
	public String getItemDate() {
		return itemDate;
	}
	
	//setter
	public void setItemDate(String itemDate) {
		this.itemDate = itemDate;
	}
	
	//toString
	public String toString() {
		return itemName + ", " + itemPrice + ", " + itemDate;
	
	
	
}
}
