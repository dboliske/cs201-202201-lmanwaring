// Lila Manwaring, CS 201, Section 1, April 29 2022
// This class connects the information of a single "item" in the market, including the name and the price.

package project;

public class Item { 
	
	//variables
	protected String itemName;
	protected String itemPrice;
	
	//default constructor
	public Item() {
		itemName = "fruit";
		itemPrice = "99.99";
	}
	
	//constructor for shelved item 
	public Item(String name, String price) {
		setItemName(name);
		setItemPrice(price);
	}
		
		
	//getters & setters
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	//toString
	public String toString() {
		return itemName + ", " + itemPrice;
	
	
	}

	
	}
	


