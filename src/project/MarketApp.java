// Lila Manwaring, CS 201, Section 1, April 29 2022
//This is the main application through which the program is run

package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;



public class MarketApp {
	
	
	static ArrayList<Item> cart = new ArrayList<Item>();  //create cart
	static ArrayList<Item> stock = new ArrayList<Item>(); //create stock

	//This method reads the file and creates an ArrayList of the Item class 
	public static void readFile(String fileName){
		
		try {
			File file = new File(fileName); //"src/project/stock.csv"	
			Scanner input = new Scanner(file);				

			while (input.hasNextLine()) {
				String itemInfo = input.nextLine();		//creates strings from data
				String[] values = itemInfo.split(","); 	//splits the values at the comma
				String firstInfo = values[0];
				String secondInfo = values[1];
				if (!(values.length > 2)) {
					Item newItem = new Item(firstInfo, secondInfo);
					stock.add(newItem);
				}
				else if (values.length > 2) {
					String thirdInfo = values[2];
					if(thirdInfo == "18" || thirdInfo == "21") { //when the third info is 18 or 21 it is an age restriction
						AgeRestricted newAgeRestricted = new AgeRestricted(firstInfo, secondInfo, thirdInfo);
						stock.add(newAgeRestricted);

					}else{												//if it is neither of those, it is a produce item
						ProduceItem newProduceItem = new ProduceItem(firstInfo, secondInfo, thirdInfo);
						stock.add(newProduceItem);
					}
				}
			}

		}catch(FileNotFoundException e){
			System.out.println("Could not find file: " + fileName);
		}
	
	}
	
	
	//This method adds items to an ArrayList (stock or cart)
	public static void addItem(ArrayList<Item> products) {
		Scanner choice = new Scanner(System.in);

		System.out.println("1. Produce Item");
		System.out.println("2. Shelf Item");
		System.out.println("3. Age Restricted Item");
		System.out.println("What type of item would you like to add?: ");
		String c = choice.nextLine();

		switch (c) {

		case "1":
			System.out.println("Enter product name: ");
			String name = choice.nextLine();
			System.out.println("Enter product price: ");
			String price = choice.nextLine();
			System.out.println("Enter product experation: ");
			String experation = choice.nextLine();
			ProduceItem newProduceItem = new ProduceItem(name, price, experation);
			products.add(newProduceItem);
			break;

		case "2":
			System.out.println("Enter product name: ");
			String name2 = choice.nextLine();
			System.out.println("Enter product price: ");
			String price2 = choice.nextLine();
			Item newItem = new Item(name2, price2);
			products.add(newItem);
			break;

		case "3":
			System.out.println("Enter product name: ");
			String name3 = choice.nextLine();
			System.out.println("Enter product price: ");
			String price3 = choice.nextLine();
			System.out.println("Enter age restriction: ");
			String age = choice.nextLine();
			AgeRestricted newAgeRestricted = new AgeRestricted(name3, price3, age);
			products.add(newAgeRestricted);

			break;
		default:
			System.out.println("Not a valid choice. Please enter '1', '2', or '3'");
		}

	}
	
	//this method modifies an individual item in stock
	public static void modifyItem() {
		
		int index = searchItem(stock);
		if (index!=-1) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("1. Produce Item");
		System.out.println("2. Shelf Item");
		System.out.println("3. Age Restricted Item");
		System.out.println("What type of item would you like to modify?");
		String c = scanner.nextLine();
		
		switch (c) {
		
		case "1":
			System.out.println("Enter name: ");
			String newName = scanner.nextLine();
			System.out.print("Enter price: ");
			String newPrice = scanner.nextLine();
			System.out.print("Enter expiration: ");
			String expiration = scanner.nextLine();
			stock.get(index).setItemName(newName);
			stock.get(index).setItemPrice(newPrice);
			((ProduceItem) stock.get(index)).setItemDate(expiration);
			break;
		case "2":
			System.out.println("Enter name: ");
			String newName1 = scanner.nextLine();
			System.out.print("Enter price: ");
			String newPrice1 = scanner.nextLine();
			stock.get(index).setItemName(newName1);
			stock.get(index).setItemPrice(newPrice1);
			break;
		case "3":
			System.out.println("Enter name: ");
			String newName2 = scanner.nextLine();
			System.out.print("Enter price: ");
			String newPrice2 = scanner.nextLine();
			System.out.println("Enter age:");
			String newAge = scanner.nextLine();
			stock.get(index).setItemName(newName2);
			stock.get(index).setItemPrice(newPrice2);
			((AgeRestricted) stock.get(index)).setAgeRestriction(newAge);
			break;
		default: 
			System.out.println("Not a valid choice. Please enter '1', '2', or '3'");
				
		
		}
		
		}
	}

	// this method searches for a user-inputed item within an ArrayList and returns the index of the item (within stock or cart)
	public static int searchItem(ArrayList<Item> list){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter Item: ");
		String desiredItem = in.nextLine();

		for (int i=0; i<list.size(); i++) {
			String itemName = list.get(i).getItemName();
			
			if(desiredItem.equalsIgnoreCase(itemName)) {
				return i;
			}
			
		}
		return -1;
	}	
		

	//This method removes a user-inputed item from an ArrayList (stock or cart)	
	public static void removeItem(ArrayList<Item> list){
		int index = searchItem(list); //finds index of product
		
		if (index != -1) {
			list.remove(index);
			System.out.println("Item removed!");
		}else {
			System.out.println("Item requested not found.");
		}
	}

	//This method displays the menu option and allows the user to move through the menu options
	public static void displayMenu() {

		Scanner choice = new Scanner(System.in);
		boolean exit = false;

		do {
			System.out.println("1. Add Item to Stock");
			System.out.println("2. Remove Item from Stock"); //input never goes anywhere,,,keeps inputting
			System.out.println("3. Add Item to Cart");
			System.out.println("4. Remove Item from Cart");
			System.out.println("5. Search for Item");
			System.out.println("6. Modify Item");	//keeps inputing too
			System.out.println("7. Exit");

			System.out.print("Choose a Command: ");

			String c = choice.nextLine();

			switch (c) {

			case "1": 

				addItem(stock);

				break;

			case "2":

				
				removeItem(stock);

				break;

			case "3": 
				
				int index = searchItem(stock); 
				cart.add(stock.get(index));		

				break;

			case "4":

				removeItem(cart);

				break;

			case "5":

				int location = searchItem(stock);
				if (location != -1){
					System.out.println("Item is in Stock");
				}else {
					System.out.println("Item is not in Stock");
				}
				break;

			case "6":
				
				modifyItem();
		

				break;

			case "7":
				
				//This prompts the user to save a current list of the stock to their own file.
				try {
					System.out.println("Do you wish to save a current list of the stock?: ");
					System.out.println("1. Yes");
					System.out.println("2. No");
					
					String yn = choice.nextLine();
					
					if (yn.equals("1")){
						
					System.out.println("File name: ");					//prompt user for file name 
					String name = choice.nextLine();	
					
					FileWriter newFile = new FileWriter(name);		//create fileWriter
					
					
					for (int i = 0; i<stock.size(); i++) {
					newFile.write(stock.get(i) + "\n"); 	//add values to file
					
					}
					
					newFile.flush();
					newFile.close();
					
					System.out.println("Saved to " + name );				//End message	
					
				} 
				}catch (IOException e) {
					System.out.println("Error");
				}
				
				//This saves the changes to the stock to the original file.
				try {
					FileWriter stockFile = new FileWriter("src/project/stock.csv");		//create fileWriter


					for (int i = 0; i<stock.size(); i++) {
					stockFile.write(stock.get(i) + "\n"); 	//add values to file

					}

					stockFile.flush();
					stockFile.close();

					}catch (IOException e) {
						System.out.println("Error is saving file.");
					}
				
				exit = true;
				 
				break;

			default:
				System.out.print("Not a Choice");
		}
			


		}while(!exit);
	}
	

	//This is the main method. It reads the file and then implements the menu method.
	public static void main(String[] args) {
		readFile("src/project/stock.csv");
		displayMenu();
		



	}

}




