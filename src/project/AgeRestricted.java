// Lila Manwaring, CS 201, Section 1, April 29 2022
// This class is a subclass of the Item class that deals with items that have an age restriction

package project;

public class AgeRestricted extends Item{ 
	
	//expiration date variable
		protected String ageRestriction;
		
		//default constructor
		public AgeRestricted(){
			super();
	
		}
		
		//non default constructor 
		public AgeRestricted(String name, String price, String age) {
				super(name, price);
				setAgeRestriction(age);
			
		}

		//getter
		public String getAgeRestriction() {
			return ageRestriction;
		}
		
		//setter
		public void setAgeRestriction(String ageRestriction) {
			if (ageRestriction == "21" || ageRestriction == "18") {
			this.ageRestriction = ageRestriction;
			}else {
				ageRestriction = "INVALID";
			}
		}
		
		
		//toString
		public String toString() {
			return itemName + ", " + itemPrice + ", " + ageRestriction;
		
			
		} 

	}



